% Authors: drs. Claudio Maccone and Nicolo' Antonietti.

clear all, close all, clc

runtime = cputime;

% Input data
t = dataGeneration;
N = length(t);
data = textread ("realkltdata.txt", "%f");

% Decide how many eigenfuctions in the KL expansion you wish to take into
% account for the reconstruction of the process X(t).
How_many_eigenfunctions = input('How many eigenfunctions ? \n');

% Computation of the ANALYTIC autocorrelation matrix of the Brownian
% motion, defined as min(t1, t2).
Autocorrelation_matrix = corr_brow(t, N);

% Eigenvectors and eigenvalues
[Phi,Lambda] = eigs(Autocorrelation_matrix,How_many_eigenfunctions);


Z = zeros(How_many_eigenfunctions,1);
KL_EXPANSIONs_ith_term = zeros(N, How_many_eigenfunctions);
for i = 1:How_many_eigenfunctions,
    Z(i) = data.' * Phi(:,i);
    KL_EXPANSIONs_ith_term(:,i) =  Z(i) * Phi(:,i);
end

% Reconstruction
EMPIRIC_Data_vector = sum(KL_EXPANSIONs_ith_term,2);

plot(t, data, t, EMPIRIC_Data_vector)