function [t] = dataGeneration

clear all, close all, clc

f = 3.5*realpow(10,6)
dt = 1 / (2*f)
n = 1000
T = dt*(n-1)
t = 0 : dt : T;


A = input ("What's the amplitude of the sine? ");

data = A * sin(2*pi*n*t);

W = input ("What's the amplitude of the noise? ");

randomwalk = wienrnd(1);
noise = randomwalk (:,2);
realdata = data + noise.';

realdata = realdata - mean(realdata);

dlmwrite ("realkltdata.txt", data, "\n");