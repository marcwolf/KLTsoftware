clear all, close all, clc

Data=[ 183, 79
       173, 69
       120, 45
       168, 70
       188, 81
       158, 61
       201, 98
       163, 63
       193, 79
       167, 71
       178, 73];
       
N=1000;

DataSub=Data-mean(Data)

cov(DataSub);

[Smat,EigValue, EigVector]=svd(Data);
 
printf("Eigen Value is: %d\n\n", diag(EigValue));

%transpose  EigVector component to be used (d<m)

EigVectorT=EigVector';

Final_Data=EigVectorT*DataSub'
figure();
    plot(DataSub(:,1),DataSub(:,2),"ob",Final_Data,"+r");
    plot(Final_Data(1,:),Final_Data(2,:),"o");