% This subroutine computes the AUTOCORRELATION of the Brownian motion B(t)
% by translating its analytical definition min(t1, t2) into a numeric
% matrix. The entries of such a matrix are each the MINIMUM between the
% relevant row and column numbers.

function C = corr_brow(t, N)

% Initializing the autocorrelation matrix

C = zeros(N);

for row = 1:N,
    for column = 1:N,
        C(row, column) = min(t(row),t(column));
    end
end