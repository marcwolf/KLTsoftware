clear all, close all, clc


pkg load image;
blocks=128;
fileIM="camras.png";
X=(double(imread(fileIM)));
width = imfinfo(fileIM).Width;
heigth = imfinfo(fileIM).Height;
X=reshape(X,width*heigth/blocks,blocks);

Xtrans=[];

for line = 1:blocks
%for line = 255:256
    CovExl=X(:,line)*X(:,line)'-mean(X(:,line))*mean(X(:,line))';
    [A,l]=eig(CovExl);
    [l_sort,perm] = sort(diag(l),"descend");
    [A] = A(:,perm);
    %Create reconstruction of vector X
    Y=A*X(:,line)-mean(X(:,line));
    Ak=A(1:128,:);
    Yr=Y(1:128,:);
    Xtrans = [Xtrans (Ak' * Yr)] ;   
end
%figure;
%imshow(uint8(X));
Xtrans=reshape(Xtrans,heigth,width);

figure
imshow(uint8(XTrans));
%plot(Xtrans(:,1),"or",X(:,255),-k);