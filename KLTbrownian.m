clear all, close all, clc

final_instant_T=1000;

time_range = (1:1:final_instant_T)';
X = process_path(final_instant_T,time_range);
X = X';

%Exl=X-mean(X);

%CovExl=(X-mean(X))*(X-mean(X))';

CovExl=X*X'-mean(X)*mean(X)';

[A,l]=eig(CovExl);
[l_sort,perm] = sort(diag(l),"descend");
[A] = A(:,perm);
%Create reconstruction of image
Y=A*X-mean(X);

%reduce transformatrix
figure;
for i=1:5
    Ak=A(1:i*200,:);
    Yr=Y(1:i*200);
    Xtrans = Ak' * Yr;

%figure('Position',[5,-50,final_instant_T,50]);
%plot(Xtrans,"-k",X+2,"-b",grid);
    subplot(5,1,i)
    plot(X-Xtrans,"-k");
    grid on;
    axis ([0,final_instant_T,-100,+100]);
    title (int2str(i));
    mean(X-Xtrans)
    %error=sum(l_sort(i*200+1:final_instant_T));
end;