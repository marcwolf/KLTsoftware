% This subroutine computes the AUTOCORRELATION of the Brownian motion B(t)
% by translating its analytical definition min(t1, t2) into a numeric
% matrix. The entries of such a matrix are each the MINIMUM between the
% relevant row and column numbers.

function C = brownian_autocorrelation(t)
n = length(t)
C = zeros(n);
for row = 1:n,
  for column = 1:n,
    C(row, column) = min(t(row),t(column));
  end
end
