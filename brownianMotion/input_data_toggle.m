% This subroutine allows you either to:
% 1) Create a brand-new REALIZATION of the input stochastic process X(t) or
% 3) Load an existing matlab file (.mat) where the input variables are
% saved
% 2) Load an existing file with all the numeric data of the input
% stochastic process. Clearly, this arises when you do EXPERIMENTAL work, 
% such as getting the input of a radiotelescope, etc.
function [time_range,final_instant_T,process_values_vector, flag] = input_data_toggle

while true,

 disp('What process is going to be analized:')
 disp('1 == A standard brownian motion from a still source')
% disp('2 == A standard brownian motion from a source in a decelerated motion')
 disp('3 == A square standard brownian motion from a still source')
% disp('4 == A square standard brownian motion from a source in decelerated motion')
 disp('5 == A uniform relativistic motion');
 
 flag = input('Enter number between 1 and 5:      ')
if flag==1 || flag==2 || flag==3 || flag==4 || flag==5 ||flag==6
   break
  end
end

while true,
  disp('Enter 1 to create a NEW REALIZATION of the Brownian motion X(t).');
  disp('Enter 2 to load an existing Brownian motion matlab file (.mat):   ');
  disp('Enter 3 to load an existing Brownian motion data file:    '); 
  case_number = input ('Enter case number:       ')

  if case_number==1 || case_number==2 || case_number==3
   break
  end
end

if case_number == 1
  % Creating the NEW REALIZATION of the stochastic process X(t)
  final_instant_T = input('Please, type the final time unit. (Suggested: no more than 1000):       ');
  time_range = (1:1:final_instant_T)';
  process_values_vector = process_path(final_instant_T);
  % Plot the Original Realization of X(t) to be later expanded and
  % reconstructed by virtue of the KLT.
  t = [0; time_range];
  random_walk = [0, process_values_vector];
  h0 = figure;
  parabola = sqrt(t);
  disp('Input_data_toggle: Plot the random walk');
  plot(t, random_walk,'-k', t,parabola,'-k', t,-parabola,'-k', t,0,'-k'), 
  title(['REALIZATION of B(t) over ', num2str(final_instant_T), ' time instants.']), 
  xlabel('time t'), 
  ylabel('X(t)')
    process_values_vector = process_values_vector';
  
    % Loading an existing matlab file (.mat) with its input stochastic
    % process.
  elseif case_number == 2
    nome = input('Please TYPE the full path and file name:   ','s');
    load(nome);
    process_values_vector = Input_Process_data;
    final_instant_T = length(Input_Process_data);
    time_range = (1:1:final_instant_T)';

    % Plot the Original Realization of X(t) to be later expanded and
    % reconstructed by virtue of the KLT.
    t = [0; time_range];
    random_walk = [0, process_values_vector'];
    h0 = figure;
    parabola = sqrt(t);
    plot(t, random_walk,'-k', t,parabola,'-k', t,-parabola,'-k', t,0,'-k'), 
    title(['REALIZATION of B(t) over ', num2str(final_instant_T), ' time instants.']), 
    xlabel('time t'), 
    ylabel('X(t)') 
    
    
    % Loading an existing EXPERIMENTALLY OBTAINED input stochastic
    % process.
  elseif case_number == 3
    nome = input('Please TYPE the full path and file name.\n','s');
    fid = fopen(nome);
    A = fscanf(fid, '%4d %4d', [2, inf]);
    A = A';
    time_range = A(:,1);
    final_instant_T = length(time_range);
    process_values_vector = A(:,2);
    fclose(fid);

    % Plot the Original Realization of X(t) to be later expanded and
    % reconstructed by virtue of the KLT.
    t = [0; time_range];
    random_walk = [0, process_values_vector'];
    h0 = figure;
    parabola = sqrt(t);
    plot(t, random_walk,'-k', t,parabola,'-k', t,-parabola,'-k', t,0,'-k'), 
    title(['REALIZATION of B(t) over ', num2str(final_instant_T), ' time instants.']), 
    xlabel('time t'), 
    ylabel('X(t)')
  end
  if flag == 1
    disp('Enter the process_valuues_vector function for flag == 1');
    process_values_vector = process_values_vector;
  elseif flag == 2
    process_values_vector = process_values_vector;
  elseif flag == 3
    process_values_vector = process_values_vector.^2 - time_range;
  elseif flag == 4
    process_values_vector = process_values_vector.^2;
  elseif flag == 5
    process_values_vector = process_values_vector;
  end
