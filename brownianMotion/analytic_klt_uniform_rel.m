% This subroutine computes the ANALYTIC RECONSTRUCTION of X(t) according to
% the analytic (either exact or approximated formulae given in the book.
function [ANALYTIC_Data_vector, tau] = analytic_klt_uniform_rel(Input_Process_data, N, How_many_eigenfunctions, t, Lambda)

ANALYTIC_Data_vector = zeros (N,1);

% Final instant T, i.e. the largest value in the "t" set.
T = N;

ratio = input('What ratio of the speed of light is the uniform velocity? (For instance 0.2):  ');
K = (1-ratio^2)^(1/4);

% This is the KEY SUBROUTINE YIELDING THE ANALYTIC RECONSTRUCTION of X(t).
for n = 1:How_many_eigenfunctions,
  gamma(n) = n*pi - pi/2;
  arg = gamma(n) * t / (T+1);
  NN = K * sqrt(2 /(T+1));
  lambda(n) = K^2 * T^2 / gamma(n)^2;
  phi = NN*sin(arg);
  zed = sum(Input_Process_data.*phi);
  ANALYTIC_Data_vector = ANALYTIC_Data_vector + zed * phi;
end

tau = sqrt(1-ratio^2)*t;
x = [1:1:How_many_eigenfunctions];

% Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).
disp('ga een figuur plotten\n');

figure;
plot(x, diag(Lambda(1:n,1:n)), '-k', x, lambda,':k'), ...
title('EIGENVALUES of the EMPIRIC and ANALYTIC Reconstruction of B(\tau).'), ...
ylabel('Eigenvalues'), 
legend('EMPIRIC eigenvalues','ANALYTIC eigenvalues')