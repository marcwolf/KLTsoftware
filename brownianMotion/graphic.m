function graphic(Input_Process_data, EMPIRIC_Data_vector, ANALYTIC_Data_vector, flag, How_many_eigenfunctions, t, N)
  disp('function: graphic()');

if flag == 1
  t = [0; t];
  Input_Process_data = [0; Input_Process_data];
  EMPIRIC_Data_vector = [0; EMPIRIC_Data_vector];
  ANALYTIC_Data_vector = [0; ANALYTIC_Data_vector];
  disp('function: graphic(): plot figure'); 
  
  plot(t, Input_Process_data,'-k', t, EMPIRIC_Data_vector,'-k', t, ANALYTIC_Data_vector,'-k'),
  title(['B(t) and its RECONSTRUCTIONS by using ', num2str(How_many_eigenfunctions), ' eigenfunctions out of ', num2str(N), '.']), 
  xlabel('time t'), ylabel('B(t)'), 
  legend('Original Realization of B(t)', 'Reconstruction by the EMPIRIC KLT', 'Reconstruction by the ANALYTIC KLT');
  hold on
  parabola = sqrt(t);
  disp('function: graphic(): end plot part I'); 
  plot(t, parabola,'-k', t, -parabola,'-k', t, 0, '-k')
  hold off
elseif flag == 2
  t = [0; t];
  Input_Process_data = [0; Input_Process_data];
  EMPIRIC_Data_vector = [0; EMPIRIC_Data_vector];
  ANALYTIC_Data_vector = [0; ANALYTIC_Data_vector];
  plot(t, Input_Process_data,'-k', t ,EMPIRIC_Data_vector,'–k', t, ANALYTIC_Data_vector,'-k'),
  title(['B(t^{2H}) and its RECONSTRUCTIONS by using ', num2str(How_many_eigenfunctions), ' eigenfunctions out of ', num2str(N), '.']),
  xlabel('time t'), ylabel('B(t^{2H})'), 
  legend('Original Realization of B(t^{2H})','Reconstruction by the EMPIRIC KLT', 'Reconstruction by the ANALYTIC KLT');
  hold on 
  parabola = sqrt(t);
  plot(t, parabola, '-k', t, -parabola, '-k', t, 0, '-k')
  hold off
elseif flag == 3
  t = [0; t];
  Input_Process_data = [0; Input_Process_data];
  EMPIRIC_Data_vector = [0; EMPIRIC_Data_vector];
  ANALYTIC_Data_vector = [0; ANALYTIC_Data_vector];
  disp('function: graphic(): plot figure'); 
  
  plot(t, Input_Process_data,'-k', t, EMPIRIC_Data_vector,'-k', t, ANALYTIC_Data_vector,'-k'),
  title(['B^2(t)-t and its RECONSTRUCTIONS by using ', num2str(How_many_eigenfunctions), ' eigenfunctions out of ', num2str(N), '.']), 
  xlabel('time t'), ylabel('B(t)'), 
  legend('Original Realization of B^2(t)-t', 'Reconstruction by the EMPIRIC KLT', 'Reconstruction by the ANALYTIC KLT');
  hold on
  parabola = sqrt(t);
  disp('function: graphic(): end plot part I'); 
  plot(t, parabola,'-k', t, -parabola,'-k', t, 0, '-k')
  hold off
elseif flag == 4
  t = [0; t];
  Input_Process_data = [0; Input_Process_data];
  EMPIRIC_Data_vector = [0; EMPIRIC_Data_vector];
  ANALYTIC_Data_vector = [0; ANALYTIC_Data_vector];
  plot(t, Input_Process_data,'-k', t ,EMPIRIC_Data_vector,'–k', t, ANALYTIC_Data_vector,'-k'),
  title(['B^2(t^{2H}) and its RECONSTRUCTIONS by using ', num2str(How_many_eigenfunctions), ' eigenfunctions out of ', num2str(N), '.']), 
  xlabel('time t'), ylabel('B^2(t^{2H})'), 
  legend('Original Realization of B^2(t^{2H})','Reconstruction by the EMPIRIC KLT', 'Reconstruction by the ANALYTIC KLT');
  hold on
  parabola = sqrt(t);
  plot(t, parabola, '-k', t, -parabola, '-k', t, 0, '-k')
  hold off
elseif flag == 5
  t = [0; t];
  Input_Process_data = [0; Input_Process_data];
  EMPIRIC_Data_vector = [0; EMPIRIC_Data_vector];
  ANALYTIC_Data_vector = [0; ANALYTIC_Data_vector];
  disp('function: graphic(): plot figure'); 
  
  plot(t, Input_Process_data,'-k', t, EMPIRIC_Data_vector,'-k', t, ANALYTIC_Data_vector,'-k'),
  title(['B(\tau) and its RECONSTRUCTIONS by using ', num2str(How_many_eigenfunctions), ' eigenfunctions out of ', num2str(N), '.']), 
  xlabel('time \tau'), ylabel('B(\tau)'), 
  legend('Original Realization of B(\tau)', 'Reconstruction by the EMPIRIC KLT', 'Reconstruction by the ANALYTIC KLT');
  hold on
  parabola = sqrt(t);
  disp('function: graphic(): end plot part I'); 
  plot(t, parabola,'-k', t, -parabola,'-k', t, 0, '-k')
  hold off
end