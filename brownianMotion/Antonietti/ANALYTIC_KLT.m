
% This subroutine computes the ANALYTIC RECONSTRUCTION of X(t) according to 
% the analytic (either exact or approximated formulae given in the book.  
function [ANALYTIC_Data_vector] = ANALYTIC_KLT(Input_Process_data, N, How_many_eigenfunctions, t, Lambda)

ANALYTIC_Data_vector = zeros (N,1);

% Final instant T, i.e. the largest value in the "t" set.
T = N;

% This is the KEY SUBROUTINE YIELDING THE ANALITIC RECONSTRUCTION of X(t). 
for n = 1:How_many_eigenfunctions,
    arg         = t.*pi * (2 * n -1) / (2*(T+1));
    NN          = sqrt(2 /(T+1));
    lambda(n)   = 4 * (T+1)^2 / (((pi)^2) * (2*n - 1)^2);
    phi         = NN*sin(arg);
    zed        = sum(Input_Process_data.*phi);
    ANALYTIC_Data_vector = ANALYTIC_Data_vector + zed * phi;
end

x = [1:1:How_many_eigenfunctions];

% Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).  
figure;
plot(x, lambda, '+', x, diag(Lambda(1:n,1:n)), '.'), title('EIGENVALUES of the EMPIRIC and ANALYTIC Reconstruction of B(t).'), ylabel('Eigenvalues');
legend('ANALYTIC eigenvalues','EMPIRIC eigenvalues')
