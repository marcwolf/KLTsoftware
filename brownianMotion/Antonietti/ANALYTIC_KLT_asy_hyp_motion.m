
% This subroutine computes the ANALYTIC RECONSTRUCTION of X^2(t) according to 
% the analytic (either exact or approximated formulae given in the book.  
function [ANALYTIC_Data_vector, tau] = ANALYTIC_KLT_asy_hyp_motion(Input_Process_data, N, How_many_eigenfunctions, t, Lambda)

ANALYTIC_Data_vector = zeros (N,1);

% Final instant T, i.e. the largest value in the "t" set.
T = N;

c = 3e8;
g = 9.8;

% Proper time for the DECELERATED motion case ("Independence Day") of a 
% relativistic spaceship approacing the Earth from the speed of light.
tau = c/g * log(2*c/g*t);

% This is the KEY SUBROUTINE YIELDING THE ANALITIC RECONSTRUCTION of X(t). 
for n = 1:How_many_eigenfunctions,
    gamma(n)    = n * pi + pi / 4;
    arg         = gamma(n) * sqrt(t/(T+1));
    NN          = sqrt(g/(c*2*(T+1)))/abs(besselj(0,gamma(n)));
    lambda(n)   = 4*c*(T+1)/(g*gamma(n)^2);
    phi         = NN.*besselj(0,arg);
    zeta        = sum(Input_Process_data.*phi);
    ANALYTIC_Data_vector = ANALYTIC_Data_vector + zeta * phi;
end

x = [1:1:How_many_eigenfunctions];

% Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).  
figure;
plot(x, lambda, '+', x, diag(Lambda(1:n,1:n)), '.'), title('EIGENVALUES of the EMPIRIC and ANALYTIC Reconstruction of B(\tau).'), ylabel('Eigenvalues');
legend('ANALYTIC eigenvalues','EMPIRIC eigenvalues')

