
% This subroutine allows you to either
% 1) Create a brand-new REALIZATION of the input stochastic process X(t) or
% 3) Load an existing matlab file (.mat) where the input variables are
% saved
% 2) Load an existing file with all the numeric data of the input
% stochastic process. Clearly, this arises when you do EXPERIMENTAL work,
% such as getting the input of a radiotelescope, etc. 


function [time_range,final_instant_T,process_values_vector, flag] = input_data_toggle

while true,
    case_number = input(" Enter 1 to create a NEW REALIZATION of the Brownian motion X(t).\n Enter 2 to load an existing Brownian motion matlab file (.mat). \n Enter 3 to load an existing Brownian motion data file. \n");

    if case_number == 1
        % Creating the NEW REALIZATION of the stochastic process X(t)
        final_instant_T = input("Please, type the final time unit. (Suggested: no more than 1000) \n");
        time_range = (1:1:final_instant_T)';
        process_values_vector = process_path(final_instant_T,time_range);
        process_values_vector = process_values_vector';
        break
        
        % Loading an existing matlab file (.mat) with its input stochastic
        % process.
    elseif case_number == 2
        nome = input("Please TYPE the full path and file name.\n",'s');
        load(nome);
        final_instant_T = length(Input_Process_data);
        time_range = (1:1:final_instant_T)';
        process_values_vector = Input_Process_data;
        break
        
        % Loading an existing EXPERIMENTALLY OBTAINED input stochastic
        % process.
    elseif case_number == 3
        nome = input("Please TYPE the full path and file name.\n",'s');
        fid = fopen(nome);
        A = fscanf(fid, '%4d %4d', [2, inf]);
        A = A';
        time_range = A(:,1);
        final_instant_T = length(time_range);
        process_values_vector = A(:,2);
        fclose(fid);
        break
        
    else
        continue
    end
end

while true,
    flag = input("What process is going to be analized? \n 1. A standard brownian motion from a still source \n 2. A standard brownian motion from a source in a decelerated motion \n 3. A square standard brownian motion from a still source \n 4. A square standard brownian motion from a source in decelerated motion \n 5. A uniform relativistic motion \n 6. An Asymptotic Hyperbolic Motion \n");
    
    if flag == 1
        process_values_vector = process_values_vector;
        break
        
    elseif flag == 2
        process_values_vector = process_values_vector;
        break
    
    elseif flag == 3
        process_values_vector = process_values_vector.^2 - time_range;
        break
        
    elseif flag == 4
        process_values_vector = process_values_vector.^2;
        break
        
    elseif flag == 5
        process_values_vector = process_values_vector;
        break
        
    elseif flag == 6
        process_values_vector = process_values_vector;
        break
    
    else
        continue
    end
end