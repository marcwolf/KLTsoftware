% Matlab 7.1 Code for the SIMULATION OF THE KLT. 
% Authors: Drs. Nicolo` Antonietti & Claudio Maccone.
% This version was completed in August 2007. 


% Clear the memory & worksheet.
clear all, close all, clc

% Define the initial value of the loop variable "runtime" as the 
% initial cpu time. At the end of this simulation, the runtime will 
% be equal to the final cpu time minus the initial cpu time. 
run_time = cputime;

% Do you prefer to run a new simulation (creating a new and different
% realization of the stochastic process X(t)) or do you wish to load an
% existing data file (produced by a previous simulation) ? 
[t, n, Input_Process_data, flag] = input_data_toggle;

% Decide how many eigenfuctions in the KL expansion you wish to take into
% account for the reconstruction of the process X(t) in the time interval
% between 0 and T. Clearly, the number of eigenfunctions taken into account
% is at most equal to the number of instants considered in the simulation.
% In practice, however, you may wish to use FEWER eigenfunctions, or even
% just VERY FEW eigenfuctions. The reconstruction of X(t) will thus be
% rougher and rougher, but the computation burden will still be affordable
% by your machine. THIS IS THE TRADE-OFF that the KLT offers to you as
% A LOSSY COMPRESSION ALGORITHM. 
How_many_eigenfunctions = input("How many eigenfunctions ? \n");
 
% Computation of the ANALYTIC autocorrelation matrix of the Brownian
% motion, defined as min(t1, t2). This autocorrelation matrix is fed into
% the code only if you previously selected to run an entirely new
% simulation. If you previously selected to load a pre-existing data file
% (as it happens in all EXPERIMENTAL applications of the KLT), then the
% data file of the values of X(t), for t ranging between 0 and T, is fed
% into the code. 
% The autocorrelation of the Brownian motion of size n is defined as 
% min(t1, t2) by % the function (i.e. by the subroutine) 
% Brownian_Autocorrelation(n), hereby called by the Main code.
%Autocorrelation_matrix = Brownian_Autocorrelation(Input_Process_data);    
Autocorrelation_matrix = corr_brow(n);
% The next step is the most important step in this Main code. 
% By virtue of the "eigs" subroutine of Matlab, we avoid getting entangled
% in the computation of the eigenvalues Lambda and of the eigenfunctions Phi
% of the KLT. Quite simply, we feed in the Autocorrelation matrix (whether
% it was ANALYTIC or NUMERIC = EXPERIMENTAL) and "eigs" returns both Lambda
% and Phi! Clearly, in non-Matlab simulations, this "eigs" routine must be
% very carefully written!
[Phi,Lambda] = eigs(Autocorrelation_matrix,How_many_eigenfunctions);
% [Phi,Lambda] = eig(Autocorrelation_matrix);
% We now compute the EMPIRIC KLT (as opposed to the ANALYTIC KLT derived in
% the book analytically) for the simulation of X(t) under consideration. 
%
% This EMPIRIC KLT we obtain in the following LOOP by:
%
% 1) PROJECTING the vector of the Input_Process_data (i.e. the vector
% representing the stochastic process X(t) to be KL-expanded) ONTO THE
% RELEVANT i-th EIGENVECTOR Phi(i). THIS PROJECTION IS THE RANDOM VARIABLE
% Z(i) of the KL expansion (as it follows by INVERTING the KL expansion, 
% just as one does for the Fourier series).  
%  
% 2) DEFINING the ith term of the KL expansion as the product of Z(i) times 
% Phi(i).
%
% for i = 1:How_many_eigenfunctions,
%     Z(i) = Input_Process_data.' * Phi(:,i);
%     KL_EXPANSIONs_ith_term(:,i) =  Z(i) * Phi(:,i);
% end

% We now create the DATA VECTOR of the EMPIRIC RECONSTRUCTION of X(t)
% achieved by the KLT numerically. This is simply the sum of all the
% KL_EXPANSIONs_ith_term obtained in the previous step of this Main code.
% EMPIRIC_Data_vector = sum(KL_EXPANSIONs_ith_term,2);

% Next we create the DATA VECTOR of the ANALYTIC RECONSTRUCTION of X(t) as
% given by the formulae mathematically demonstrated in the book. 
% This requires a separate routine (named hereafter ANALYTIC_KLT) to be
% called up by this Main code. The text of this rou◘tine clearly changed
% according to which formula in the book we refer to. 
if flag == 1
    [ANALYTIC_Data_vector] = ANALYTIC_KLT(Input_Process_data, n, How_many_eigenfunctions, t, Lambda);
elseif flag == 2
    [ANALYTIC_Data_vector] = ANALYTIC_KLT_decelerated(Input_Process_data, n, How_many_eigenfunctions, t, Lambda);
elseif flag == 3
    [ANALYTIC_Data_vector] = ANALYTIC_KLT_square_brow_motion(Input_Process_data,n,How_many_eigenfunctions, t, Lambda);
elseif flag == 4
    [ANALYTIC_Data_vector] = ANALYTIC_KLT_square_brow_dec_motion(Input_Process_data,n,How_many_eigenfunctions, t, Lambda);
elseif flag == 5
    [ANALYTIC_Data_vector, t] = ANALYTIC_KLT_uniform_rel(Input_Process_data,n,How_many_eigenfunctions, t, Lambda);
elseif flag == 6
    [ANALYTIC_Data_vector, t] = ANALYTIC_KLT_asy_hyp_motion(Input_Process_data,n,How_many_eigenfunctions, t, Lambda);
end
    
for i = 1:How_many_eigenfunctions,
    Z(i) = Input_Process_data.' * Phi(:,i);
    KL_EXPANSIONs_ith_term(:,i) =  Z(i) * Phi (:,i); %Lambda(i) * Phi(:,i);
end

EMPIRIC_Data_vector = sum(KL_EXPANSIONs_ith_term,2);

% Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).
h2 = figure;

% Plots of: 
% 1) The ORIGINAL REALIZATION of X(t). 
% 2) The EMPIRIC RECONSTRUCTION OF X(t) by the KLT. 
% 3) The ANALYTIC RECONSTRUCTION OF X(t) by the KLT.
graphic(Input_Process_data, EMPIRIC_Data_vector, ANALYTIC_Data_vector, flag, How_many_eigenfunctions, t, n)


% plot(t, Input_Process_data,'r', t ,EMPIRIC_Data_vector,'b', t, ANALYTIC_Data_vector,'k'), 
% title(['X(t) and its RECONSTRUCTIONS by using ', num2str(How_many_eigenfunctions), ' eigenfunctions out of ', num2str(n), '.']),
% xlabel('time t'), ylabel('X(t)'), legend('Original Realization of X(t)', 'Reconstruction by the EMPIRIC KLT', 'Reconstruction by the ANALYTIC KLT') 
% hold on
% parabola = sqrt(t);
% plot(t, parabola, 'g', t, -parabola, 'g', t, 0, 'g')
% hold off

% Save the Input_Process_data as the Matlab file i01mat. 
save i01.mat Input_Process_data

% How long it took to do all these calculations.
run_time = cputime - run_time;

m = mean(Input_Process_data);