
% This subroutine computes the ANALYTIC RECONSTRUCTION of X(t) according to 
% the analytic (either exact or approximated formulae given in the book.  
function [ANALYTIC_Data_vector] = ANALYTIC_KLT_decelerated(Input_Process_data, N, How_many_eigenfunctions, t, Lambda)

ANALYTIC_Data_vector = zeros (N,1);

% Final instant T, i.e. the largest value in the "t" set.
T = N;

% H is the so-called "Hurst parameter" that, in this case of the
% DECELARATED spaceship motion, specifies whether the deceleration is
% "sudden" (i.e. it lasts for a short time only) or "soft" (i.e. it takes a
% longer time to stop). 
H = input("Value of Hurst parameter H > 0.75 ? (Suggested: 0.8) \n");

% Analytic expression of the constant C as given in the book, eq. (17.47).
C = 1/(2*H*T^(2*H -1))

% Proper time for the DECELERATED motion case ("Independence Day") of a 
% relativistic spaceship approacing the Earth from the speed of light.
tau = C * t.^(2 * H);

% This is the KEY SUBROUTINE YIELDING THE ANALITIC RECONSTRUCTION of X(t). 
for n = 1:How_many_eigenfunctions,
    gamma(n)    = n * pi - pi / 4 - pi / (2*(2*H+1));
    nu          = 2*H/(2*H+1);
    arg         = gamma(n) * t.^(H+1/2)/(T+1)^(H+1/2);
    NN          = sqrt(2*H+1) * (t.^H) / (T+1)^(H+1/2);
    lambda(n)   = (T+1)^(2*H+1) / ((H+1/2)^2) / (gamma(n)^2);
    phi         = NN.* besselj(nu,arg) / abs(besselj(nu,gamma(n)));
    zeta        = sum(Input_Process_data.*phi);
    ANALYTIC_Data_vector = ANALYTIC_Data_vector + zeta * phi;
end

x = [1:1:How_many_eigenfunctions];

% Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).  
figure;
plot(x, lambda, '+', x, diag(Lambda(1:n,1:n)), '.'), title('EIGENVALUES of the EMPIRIC and ANALYTIC Reconstruction of B(t^{2H}).'), ylabel('Eigenvalues');
legend('ANALYTIC eigenvalues','EMPIRIC eigenvalues')

