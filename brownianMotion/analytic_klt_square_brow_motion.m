% This subroutine computes the ANALYTIC RECONSTRUCTION of X(t) according to
% the analytic (either exact or approximated formulae given in the book.
function [ANALYTIC_Data_vector] = analytic_klt_square_brow_motion(Input_Process_data, N,
How_many_eigenfunctions, t, Lambda)

ANALYTIC_Data_vector = zeros (N,1);

% Final instant T, i.e. the largest value in the "t" set.
T = N;

%Input_Process_data = Input_Process_data - mean(Input_Process_data);

% This is the KEY SUBROUTINE YIELDING THE ANALYTIC RECONSTRUCTION of X^2(t).
 
for n = 1:How_many_eigenfunctions,
  gamma(n) = pi * (n-5/12);
  arg = gamma(n) * t.^(3/2)/(T+1)^(3/2);
  NN = sqrt(3)*t /(T+1)^(3/2) / abs(besselj(2/3, gamma(n)));
  lambda(n) = 16/9*(T+1)^3 / ((gamma(n))^2);
  phi = NN.*besselj(2/3,arg);
  zeta = sum(Input_Process_data.*phi);
  ANALYTIC_Data_vector = ANALYTIC_Data_vector + zeta * phi ;
end

x = [1:1:How_many_eigenfunctions];

% Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).
figure;
plot(x, diag(Lambda(1:n,1:n)), '-k', x, lambda,':k'), ...
title('EIGENVALUES of the EMPIRIC and ANALYTIC Reconstruction of X(t).'), ...
ylabel('Eigenvalues'), legend('EMPIRIC eigenvalues','ANALYTIC eigenvalues')
