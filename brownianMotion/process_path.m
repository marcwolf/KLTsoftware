% Subroutine creating the RANDOM WALK path of the Brownian motion B(t).
function X = process_path(T)

% Set to zero all the initial values of the T-element vector B that will
% contain the (random) values of the X(t) process when the new realization
% of X(t) will have been computed.
X = zeros(1,T);

% Create a vector with random entries and as many elements as are the time
% instants between 1 and the final instant T.
random_vector = rand(1,T);

for i = 1:T
  while random_vector(i) == 0.5
    random_vector(i) = rand(1);
  end

  if random_vector(i) < 0.5
    Increment(i) = - 1;
  elseif random_vector(i) > 0.5
  Increment(i) = + 1;
  end
  if i == 1
    % X(i) = 0 + Increment(i) + 2*sin(i/pi);
    X(i) = 0 + Increment(i);
  else
    % X(i) = X(i-1) + Increment(i) + 2*sin(i/pi);
    X(i) = X(i-1) + Increment(i);
  end
end
