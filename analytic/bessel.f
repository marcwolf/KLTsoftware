    real*8 function besselj(n,x)
    implicit none
    
c compute the bessel of the first kind, with real order
c arguments
    real*8n,x
c locals
    integerreal*8i
    m,s,s1,s2,s3
    s=0.
    do i=0,100
      m=1.*i
      s1 = (-1.)**m / gamma(m+1.)
      s2 = gamma(m+n+1)
      s3 = (x/2.)**(2*m+n)
      s=s+s1/s2*s3
    enddo
    besselj = s
    return
    end
