    subroutine input(time_range,final_t,process_vector,flag)
    implicit none
c this subroutine allows you to either
c 1) create a brand-new realization of the input stochastic process
c x(t) or
c 2) load an existing file with all the numeric data of the input
c stochastic process. clearly, this arises when you do experimental
c work, such as getting the input of a radiotelescope, etc.

    include ’brownian.inc’

c arguments
    integerreal*8flag,final_t
    time_range(max_size),process_vector(max_size)

    c internal

    integer i,j,case_number
    character*256 name
10
    write(*,*) ’what process is going to be analized?’
    write(*,*) ’ 1. a standard brownian motion from a still source’
    write(*,*) ’ 2. a standard brownian motion from a source in a
      & decelerated motion’
    write(*,*) ’ 3. a square standard brownian motion from a still
      & source’
    write(*,*) ’ 4. a square standard brownian motion from a source
      & in decelerated motion’
    write(*,*) ’ 5. a uniform relativistic motion’
    read(*,*) flag
    if (flag.lt.1 .or. flag.gt.5) goto 10
20  write(*,*) ’enter 1 to create a new realization of the brownian & motion x(t).’
    write(*,*) ’enter 2 to load an existing brownian motion data file.’
    read(*,*) case_number if (case_number.lt.1 .or. case_number.gt.2) goto 20

c creating the new realization of the stochastic process X(t)

    if (case_number.eq.1) then
      write(*,*) ’please, type the final time unit. (suggested: no & more than 1000)’
      read(*,*) final_t
      do i=1,final_t
	time_range(i)=1.*i
      enddo
      call process(final_t,process_vector)

c plot the original realization of X(t) to be later expanded and
c reconstructed by virtue of the KLT.

    open(unit=10,file=’plot_input’)
    do i=1,final_t
      write(10,1000) i,process_vector(i),sqrt(1.*i),-sqrt(1.*i)
    enddo
    close(10)
c loading an existing experimentally obtained input stochastic process.

    else if (case_number.eq.2) then
      write(*,*) ’please type the full path and file name.’
      read(*,*) name
30
35    open(unit=10,file=name)
      i=1
      read(10,*,end=35) j,process_vector(i)
      i=i+1
      goto 30
      close(10)
      final_t = i-1
      do i=1,final_t
	time_range(i)=1.*i
      enddo
      open(unit=10,file=’plot_input’)
      do i=1,final_t
	write(10,1000) i,process_vector(i),sqrt(1.*i),-sqrt(1.*i)
      enddo
      close(10)
    endif
    
    if (flag .eq. 3) then
      do i=1,final_t
	process_vector(i)=process_vector(i)**2-time_range(i)
      enddo
    else if (flag .eq. 4) then
      do i=1,final_t
	process_vector(i)=process_vector(i)**2
      enddo
    endif
1000 format(i5,4e15.4)
    return
    end

