subroutine eigenvalue(a,n,phi,lambda)
implicit none

c compute the eigenvalues and eigenvectors of matrix a

include ’brownian.inc’

c argument
integerreal*8real*8real*8n
a(max_size,max_size)
phi(max_size,max_size)
lambda(max_size)
clocals
integernrot
c JACOBI and EIGSRT are routines taken from Numerical Recipes
c  Licensing forbids the redistribution of their code
c and it is not possible to give it here.
c The user must get the appropriate subroutines, compile them
c and link them.
c Other mathematical libraries offers such subroutines. They can
c be used instead of those here.
c please refer to the documentation of Numerical Recipes for
c more details.
c phi() contains the eigenvectors
c phi(i,j) refers to the i-th element of the j-th eigenvector
c JACOBI is the subroutine responsible for computing eigenvalues
c and eigenvectors.

call jacobi(a,n,max_size,lambda,phi,nrot)

c EIGSRT sorts the output of JACOBI so that the eigenvalues
c are from the largest to the smallest. The eigenvectors are also
c sorted accordingly.

call eigsrt(lambda,phi,n,max_size)
return
end
