c -------------------------------------------------------------

    subroutine analytic_klt(input_data,n,neigs,t,lambda,dvector)
    implicit none

c This subroutine computes the ANALYTIC RECONSTRUCTION of X(t)
c according to the analytic (either exact or approximated formulae
c given in the book.)

    include ’brownian.inc’

c arguments

    integerreal*8n,neigs
    input_data(n),t(n),lambda(n),dvector(n)
c locals

    integerreal*8i,j
    arg(n),c1,zed,phi(n),nn,lda(n)
    do i=1,n
      dvector(i)=0.
    enddo
c This is the KEY SUBROUTINE YIELDING THE ANALYTIC RECONSTRUCTION
c of X(t).

    do i=1,neigs
      do j=1,n
	arg(j)=t(j)*pi*(2*i-1)/(2*(n+1))
      enddo
      nn = sqrt(2/(n+1.))
      lda(i) = 4*(n+1)**2./(pi**2*(2*i-1)**2)
      do j=1,n
	phi(j)=nn*sin(arg(j))
      enddo
      zed=0.
      do j=1,n
	zed=zed+input_data(j)*phi(j)
      enddo
      do j=1,n
	dvector(j)=dvector(j)+zed*phi(j)
      enddo
    enddo

c Plot the EIGENVALUES of the EMPIRIC AND ANALYTIC RECONSTRUCTIONS
c of X(t).

    open(unit=10,file=’plot_lambda’)

    do i=1,neigs
      write(10,1000) i,lda(i),lambda(i) ! TO BE CHECKED
    enddo
    close(10)
1000 format(i5,2e15.5)
    return
    end
c ------------------------------------------------------------------

    subroutine analytic_decel(input_data,n,neigs,t,lambda,dvector)
    implicit none

c This subroutine computes the ANALYTIC RECONSTRUCTION of X(t)
c according to the analytic (either exact or approximated formulae
c given in the book.)

    include ’brownian.inc’

c arguments
    integerreal*8n,neigs
    input_data(n),t(n),lambda(n),dvector(n)

clocals

    integerreal*8real*8i,j
    nn(n),lda(n),arg(n),gam(neigs),phi(n),tau(n)
    h,c,nu,zeta

c external

    real*8 besselj
    external besselj
    do i=1,n
      dvector(i)=0.
    enddo
c H is the so-called "Hurst parameter" that, in this case of the
c DECELARATED spaceship motion, specifies whether the deceleration is
c "sudden" (i.e. it lasts for a short time only) or "soft" (i.e. it
c takes a longer time to stop).

    write(*,*) ’Value of Hurst parameter H>0.75 ? (Suggested: 0.8)’
    read(*,*) h

c Analytic expression of the constant C as given in the book,
c eq. (17.47).
c = 1./(2.*h*n**(2*h-1))
c Proper time for the DECELERATED motion case ("Independence Day")
c of a relativistic spaceship approaching the Earth at the speed
c of light.

    do i=1,n
      tau(i)=c*t(i)**(2*h)
    enddo

c This is the KEY SUBROUTINE YIELDING THE ANALYTIC RECONSTRUCTION
c of X(t).

    do i=1,neigs
      gam(i) = i*pi - pi/4. - pi/(2*(2*h+1))
      nu  = 2*h/(2*h+1)
      do j=1,n
	arg(j)=gam(i)*t(j)**(h+0.5)/(n+1)**(h+0.5)
	nn(j)=sqrt(2*h+1)*t(j)**h/(n+1)**(h+0.5)
      enddo
      lda(i)=(n+1)*(2*h+1)/(gamma(h+1./2.)**2)/(gam(i)**2)
      zeta = 0.
      do j=1,n
	phi(j) = nn(j)*besselj(nu,arg(j))/abs(besselj(nu,gam(i)))
	zeta=zeta+input_data(j)*phi(j)
      enddo
      do j=1,n
	dvector(j)=dvector(j)+zeta*phi(j)
      enddo
    enddo
    
c Plot the EIGENVALUES of the EMPIRIC AND ANALYTIC RECONSTRUCTION of X(t).
    open(unit=10,file=’plot_lambda’)
    do i=1,neigs
      write(10,1000) i,lda(i),lambda(i)
    enddo
    close(10)
1000 format(i5,2e15.5)
    return
    end
    
c ----------------------------------------

    subroutine analytic_brow(input_data,n,neigs,t,lambda,dvector)
    implicit none
    
c This subroutine computes the ANALYTIC RECONSTRUCTION of X(t)
c according to the analytic (either exact or approximated formulae
c given in the book.)

    include ’brownian.inc’
    
c arguments
    integerreal*8n,neigs
    input_data(n),t(n),lambda(n),dvector(n)

c locals
    integerreal*8real*8i,j
    nn(n),vector(n),lda(n),phi(n),gam(neigs),arg(n)
    zeta,order
c external

    real*8 besselj
    external besselj
    do i=1,n
      dvector(i)=0.0
    enddo
      order = 2./3.
    do i=1,neigs
      gam(i) = pi * (i-5./12.)
      do j=1,n
	arg(j)=gam(i)*t(j)**(3./2.)/(n+1)**(3./2.)
	nn(j)=sqrt(3.)*t(j)/(n+1)**(3./2.)/abs(besselj(order,gam(i)))
      enddo
      lda(i) = 16./9.*(n+1)**3./gam(i)**2.
      zeta=0.
      do j=1,n
	phi(j)=nn(j)*besselj(order,arg(j))
	zeta=zeta+input_data(j)*phi(j)
      enddo
      do j=1,n
	dvector(j)=dvector(j)+zeta*phi(j)
      enddo
    enddo
    
c Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).

    open(unit=10,file=’plot_lambda’)
    do i=1,neigs
      write(10,1000) i,lda(i),lambda(i)
    enddo
    close(10)
1000 format(i5,2e15.5)
    return
    end
    
c ---------------------------------------------
    subroutine analytic_brow_dec(input_data,n,neigs,t,lambda,dvector)
    implicit none
c This subroutine computes the ANALYTIC RECONSTRUCTION of X^2(t)
c according to the analytic (either exact or approximated formulae
c given in the book.

    include ’brownian.inc’

c arguments
    integerreal*8n,neigs
    input_data(n),t(n),lambda(n),dvector(n)

clocals
    integerreal*8real*8i,j
    tau(n),gam(neigs),phi(n),arg(n),nn(n),lda(n)
    h,c,nu,zeta
    do i=1,n
      dvector(i)=0.
    enddo
c H is the so-called "Hurst parameter" that, in this case of the
c DECELERATED spaceship motion, specifies whether the deceleration is
c "sudden" (i.e. it lasts for a short time only) or "soft" (i.e. it
c takes a longer time to stop).

    write(*,*) ’Value of Hurst parameter H > 0.75 ? (Suggested: 0.8)’
    read(*,*) h
c Analytic expression of the constant C as given in the book,
c eq. (17.47).
c = 1./(2.*H*n**(2.*H-1))
c Proper time for the DECELERATED motion case ("Independence Day") of a
c relativistic spaceship approaching the Earth at the speed of light.

    do j=1,n
      tau(j)=c*t(j)**(2.*h)
    enddo
    do i=1,neigs
      gam(i) = i*pi - pi/4. - pi/(2.*(2*h+1))
      nu = 2.*h/(2.*h+1./2.)
      do j=1,n
	arg(j)=gam(i)*t(j)**(h+1./2.)/(n+1)**(h+1./2.)-2.*h*pi/(2.*(2.*h+1))-pi/4.
	nn(j)=sqrt(2*h+1)*t(j)**(h/2.-0.25)/(n+1)**(h/2.+0.25)
      enddo
      lda(i) = (n+1)**2/((h+0.5)**2 / gam(i)**2)
      zeta = 0.
      do j=1,n
	phi(j)=nn(j)*sin(arg(j))
	zeta=zeta+input_data(j)*phi(j)
      enddo
      do j=1,n
	dvector(j)=dvector(j)+zeta*phi(j)
      enddo
    enddo
    
c Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).
    open(unit=10,file=’plot_lambda’)
    do i=1,neigs
      write(10,1000) i,lda(i),lambda(i)
    enddo
    close(10)
1000 format(i5,2e15.5)
    return
    end
c----------------------------------------

    subroutine analytic_uniform(input_data,n,neigs,t,lambda,dvector)
    implicit none

c This subroutine computes the ANALYTIC RECONSTRUCTION of X(t)
c according to the analytic (either exact or approximated formulae
c given in the book.)

    include ’brownian.inc’
c arguments

    integer n,neigs
    real*8 input_data(n),t(n),lambda(n),dvector(n)

c locals
    integer i,j
    real*8 ratio,k,gam(neigs),zed,lda(n),phi(n),arg(n),nn(n)
    write(*,*) ’What ratio of the speed of light is the uniform & velocity? (For instance 0.2)’
    read(*,*) ratio
    k = (1.0-ratio**2.0)**(0.25)
    do i=1,n
      dvector(i)=0.
    enddo
    do i=1,neigs
      gam(i) = i*pi - pi/2.0
    do j=1,n
      arg(j)=gam(i)*t(j)/(n+1)
      nn(j)=k*sqrt(2./(n+1))
    enddo
    lda(i) = k**2.0 * n**2.0 / gam(i)**2
    zed = 0.
    do j=1,n
      phi(j)=nn(j)*sin(arg(j))
      zed=zed+input_data(j)*phi(j)
    enddo
    do j=1,n
      dvector(j)=dvector(j)+zed*phi(j)
    enddo
    enddo
c Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).
    open(unit=10,file=’plot_lambda’)
    do i=1,neigs
      write(10,1000) i,lda(i),lambda(i)
    enddo
    close(10)
1000 format(i5,2e15.5)
    return
    end
