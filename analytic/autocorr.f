    subroutine autocorr(t,n,c)
    implicit none

c this subroutine computes the autocorrelation of the brownian motion
c b(t) by translating its analytical definition min(t1, t2) into a
c numeric matrix. the entries of such a matrix are each the minimum
c between the relevant row and column numbers.

    include ’brownian.inc’
c arguments

    integer n
    real*8 t(max_size),c(max_size,max_size)
c locals

    integer row,col
    do row=1,n
      do col=1,n

c(row,col)=min(row,col)
      enddo
    enddo
    return
    end
