    subroutine process(t,x)
    implicit none
c subroutine creating the random walk path of the brownian motion b(t).

    include ’brownian.inc’
c arguments
    integerreal*8t
    x(max_size)
c locals
    integerreal*8i
    rnd

c set to zero all the initial values of the t-element vector b that will
c contain the (random) values of the X(t) process when the new realization
c of x(t) will have been computed.

    x(1)=0.0
    do i=2,t
10
      rnd=rand()
      if (rnd.eq.0.5) goto 10
	if (rnd.gt.0.5) then
	  x(i)=x(i-1)-1.0
	else
	  x(i)=x(i-1)+1.0
      endif
    enddo
    return
    end
