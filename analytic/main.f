c FORTRAN code for the simulation of the KLT.
c based on the matlab code from drs. Nicolo Antonietti & Claudio Maccone.
c FORTRAN version by Stephane Dumas

    program brownian
    implicit none include ’brownian.inc’
c locals
    integer n,neigs,flag
    integer i,j
    real*8 t(max_size),input_data(max_size)
    real*8 amat(max_size,max_size)
    real*8 phi(max_size,max_size)
    real*8 lambda(max_size)
    real*8 klexp(max_eig,max_size)
    real*8 z(max_size)
    real*8 advector(max_size) ! analytic data vector
    real*8 edvector(max_size) ! empiric data vector

c do you prefer to run a new simulation (creating a new and different
c realization of the stochastic process x(t)) or do you wish to load an
c existing data file (produced by a previous simulation) ?

    call input(t,n,input_data,flag)
c decide how many eigenfuctions in the KL expansion you wish to take into
c account for the reconstruction of the process X(t) in the time interval
c between 0 and T. clearly, the number of eigenfunctions taken into account
c is at most equal to the number of instants considered in the simulation.
c in practice, however, you may wish to use fewer eigenfunctions, or even
c just very few eigenfuctions. the reconstruction of X(t) will thus be
c rougher and rougher, but the computation burden will still be affordable
c by your machine. this is the trade-off that the KLT offers to you as
c a lossy compression algorithm.

    write(*,*) ’how many eigenfunctions ?’
    read(*,*) neigs
c computation of the analytic autocorrelation matrix of the brownian
c motion, defined as min(t1, t2). this autocorrelation matrix is fed into
c the code only if you previously selected to run an entirely new
c simulation. if you previously selected to load a pre-existing data file
c (as happens in all experimental applications of the KLT), then the
c data file of the values of x(t), for t ranging between 0 and t, is fed
c into the code.
c the autocorrelation of the brownian motion of size n is defined as
c min(t1, t2) by the function (i.e. by the subroutine)
c brownian_autocorrelation(n), hereby called by the main code.
c (was) autocorrelation_matrix = brownian_autocorrelation(t);

call autocorr(input_data,n,amat)

c the next step is the most important step in this main code.
c by virtue of the "eigs" subroutine of matlab, we avoid getting entangled
c in the computation of the eigenvalues lambda and of the eigenfunctions phi
c of the KLT. quite simply, we feed in the autocorrelation matrix (whether
c it was analytic or numeric = experimental) and "eigs" returns both lambda
c and phi! clearly, in non-matlab simulations, this "eigs" routine must be
c very carefully written!

    call eigenvalue(amat,n,phi,lambda)

c we now compute the empiric KLT (as opposed to the analytic KLT derived in
c the book analytically) for the simulation of X(t) under consideration.
c
c this empiric KLT we obtain in the following loop by:
c
c 1) projecting the vector of the input_process_data (i.e. the vector
c representing the stochastic process X(t) to be KL-expanded) onto the
c relevant i-th eigenvector phi(i). this projection is the random variable
c Z(i) of the KL expansion (as follows by inverting the KL expansion,
c just as one does for the fourier series).
c
c 2) defining the ith term of the kl expansion as the product of Z(i) times
c phi(i).

    do i=1,neigs
      z(i) = 0.
      do j=1,n
	z(i) = z(i) + input_data(j)*phi(j,i)
      enddo
      
      do j=1,n
	klexp(i,j)=z(i)*phi(j,i)
      enddo
    enddo

c we now create the data vector of the empiric reconstruction of x(t)
c achieved by the KLT numerically. this is simply the sum of all the
c kl_expansions_ith_term obtained in the previous step of this main code.

    do i=1,n
      edvector(i)=0.0
    enddo
    do i=1,neigs
      do j=1,n
	edvector(j)=edvector(j)+klexp(i,j)
      enddo
    enddo

c next we create the data vector of the analytic reconstruction of X(t) as
c given by the formulae mathematically demonstrated in the book.
c this requires a separate routine (named hereafter analytic_klt) to be
c called up by this main code. the text of this routine clearly changed
c according to which formula in the book we refer to.

    if (flag.eq.1) then
      call analytic_klt(input_data,n,neigs,t,lambda,advector)
    
    else if (flag.eq.2) then
      call analytic_decel(input_data,n,neigs,t,lambda,advector)
    else if (flag.eq.3) then
      call analytic_brow(input_data,n,neigs,t,lambda,advector)
    else if (flag.eq.4) then
      call analytic_brow_dec(input_data,n,neigs,t,lambda,advector)
    else if (flag.eq.5) then
      call analytic_uniform(input_data,n,neigs,t,lambda,advector)
    endif
c Plot the EIGENVALUES of the EMPIRIC RECONSTRUCTION of X(t).
c Plots of:
c 1) The ORIGINAL REALIZATION of X(t)
c 2) The EMPIRIC RECONSTRUCTION OF X(t) by the KLT.
c 3) The ANALYTIC RECONSTRUCTION OF X(t) by the KLT.

    open(unit=10,file=’plot_final’)
    do i=1,n
      write(10,1000) i,input_data(i),sqrt(1.*i),-sqrt(1.*i),&  advector(i),edvector(i)
    enddo
    close(10)

    1000 format(i5,5e15.4)
    end
