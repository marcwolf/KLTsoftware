function xy = ellipse( r, center, phi )
%
%  xy = ellipse( r, center, angle ) - return points on ellipse
%
%  r      - radii
%  center - center of ellipse
%  phi    - rotation angle of ellipse
%

	
	theta = (0:100)' * 2 * pi / 100.0;
	xy = [ r(1) * cos(theta), r(2) * sin(theta)];
	r = [cos(phi), -sin(phi); sin(phi), cos(phi)];
	xy = (r * xy')';
	xy(:,1) = xy(:,1) + center(1);
	xy(:,2) = xy(:,2) + center(2);
